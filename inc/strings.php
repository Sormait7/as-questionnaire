<?php 
add_action( 'plugins_loaded', 'asq_register_strings' );
function asq_register_strings(){
	pll_register_string('FIO', 'FIO', 'Анкеты' );
	pll_register_string('ADDRESS', 'ADDRESS', 'Анкеты' );
	pll_register_string('PHONE', 'PHONE', 'Анкеты' );
	pll_register_string('EMAIL', 'EMAIL', 'Анкеты' );
	pll_register_string('CONTACT', 'CONTACT', 'Анкеты' );
	pll_register_string('R_PHONE', 'R_PHONE', 'Анкеты' );
	pll_register_string('R_MAIL', 'R_MAIL', 'Анкеты' );
	pll_register_string('PKU', 'PKU', 'Анкеты' );
	pll_register_string('PATIENT', 'PATIENT', 'Анкеты' );
	pll_register_string('BIRTHDATE', 'BIRTHDATE', 'Анкеты' );
	pll_register_string('HEIGHT', 'HEIGHT', 'Анкеты' );
	pll_register_string('WEIGHT', 'WEIGHT', 'Анкеты' );
	pll_register_string('DELIVERY', 'DELIVERY', 'Анкеты' );
	pll_register_string('PRODUCTS', 'PRODUCTS', 'Анкеты' );
	pll_register_string('HOSPITAL', 'HOSPITAL', 'Анкеты' );
	pll_register_string('WISHES', 'WISHES', 'Анкеты' );
	pll_register_string('BUTTON_TEXT', 'BUTTON_TEXT', 'Анкеты' );

	pll_register_string('ERROR_MESSAGE', 'ERROR_MESSAGE', 'Анкеты' );
	pll_register_string('SUCCESS_MESSAGE', 'SUCCESS_MESSAGE', 'Анкеты' );
	pll_register_string('ASL_USER_STATUS_ERROR', 'ASL_USER_STATUS_ERROR', 'Анкеты' ); // to do this action user must be logged out
}
?>