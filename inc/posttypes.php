<?php 
defined( 'ABSPATH' ) or die();

function asq_register_posttypes(){
	register_post_type('questionnaire', array(
		'labels'             => array(
			'name'               => 'Анекты',
			'singular_name'      => 'Анкета',
			'add_new'            => 'Добавить анкету',
			'add_new_item'       => 'Добавить новую анкету',
			'edit_item'          => 'Редактировать анкету',
			'new_item'           => 'Новая анкета',
			'view_item'          => 'Посмотреть анкету',
			'search_items'       => 'Найти анекту',
			'not_found'          => 'Анкет не найдено',
			'not_found_in_trash' => 'В корзине анкеты не найдены',
			'menu_name'          => 'Анкеты'
		  ),
		'public'             => false,
		'hierarchical'       => false,
		'menu_position'      => 30,
		'supports'           => array('title','editor'),
		'exclude_from_search' => true,
		'show_ui' 			=> true,
		'show_in_menu' 		=> true,
		'menu_icon' 		=> 'dashicons-media-document',
	) );
}
add_action( 'init', 'asq_register_posttypes' );