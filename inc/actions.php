<?php 
// checking and handling if the question form was submited
// add_action( 'admin_post_nopriv_qa_add_question', 'asqa_handle_question_form' );
add_action( 'wp_ajax_save_questionnaire', 'as_save_questionnaire' );
add_action( 'wp_ajax_nopriv_save_questionnaire', 'as_save_questionnaire' );
function as_save_questionnaire(){
	if ( !empty( $_POST ) && isset( $_POST['csrf'] ) ){
		if( wp_verify_nonce( $_POST['csrf'], 'save_questionnaire' ) ){
			/**
			 * available fields:
			 * - fio
			 * - address
			 * - email
			 * - contact
			 * - pku
			 * - patient
			 * - birthdate
			 * - height
			 * - weight
			 * - delivery
			 * - products
			 * - hospital
			 * - wishes
			 */

			// check if user is logged in ( must be logged out )
			if( 0 != get_current_user_id() ){
				echo json_encode( [ 'status' => 0, 'message' => pll__( 'ASL_USER_STATUS_ERROR' ) ] );
				wp_die();
			}

			$post_title = sanitize_text_field( $_POST['fio'] );

			$post_content = '';

			$post_content .= '<p><strong>Адреса проживання</strong>: '.sanitize_text_field( $_POST['address'] ).'</p>';
			$post_content .= '<p><strong>Телефон</strong>: '.sanitize_text_field( $_POST['phone'] ).'</p>';
			$post_content .= '<p><strong>Електронна адреса</strong>: '.sanitize_email( $_POST['email'] ).'</p>';
			$post_content .= '<p><strong>Cпосіб зв’язатися</strong>: '.sanitize_text_field( $_POST['contact'] ).'</p>';
			$post_content .= '<p><strong>Яке відношення до ФКУ</strong>: '.sanitize_text_field( $_POST['pku'] ).'</p>';
			$post_content .= '<p><strong>ПІП пацієнта</strong>: '.sanitize_text_field( $_POST['patient'] ).'</p>';
			$post_content .= '<p><strong>Дата, рік народження</strong>: '.sanitize_text_field( $_POST['birthdate'] ).'</p>';
			$post_content .= '<p><strong>Зріст, см</strong>: '.sanitize_text_field( $_POST['height'] ).'</p>';
			$post_content .= '<p><strong>Вага, кг</strong>: '.sanitize_text_field( $_POST['weight'] ).'</p>';
			$post_content .= '<p><strong>Найкращий спосіб доставки</strong>: '.sanitize_text_field( $_POST['delivery'] ).'</p>';
			$post_content .= '<p><strong>Які низькобілкові продукти Ви купуєте</strong>: '.sanitize_text_field( $_POST['products'] ).'</p>';
			$post_content .= '<p><strong>Лікувальний заклад</strong>: '.sanitize_text_field( $_POST['hospital'] ).'</p>';
			$post_content .= '<p><strong>Побажання</strong></strong>: '.sanitize_text_field( $_POST['wishes'] ).'</p>';

			$pid = wp_insert_post( [
				'post_title' => $post_title,
				'post_content' => $post_content,
				'post_status' => 'draft',
				'post_author' => 1,
				'post_type' => 'questionnaire',
			], 0 );


			if( is_wp_error( $pid ) || !$pid ){
				echo json_encode( [ 'status' => 0, 'message' => pll__( 'ERROR_MESSAGE' ) ] );
				wp_die();
			}
			else{
				// check if somebody has phone
				// $users = get_users( array(
				//     'billing_phone' => sanitize_text_field( $_POST['phone'] ),
				//     'shipping_phone' => sanitize_text_field( $_POST['phone'] ),
				// ) );

				// var_dump( $users );

				// if( !empty( $users ) ){
				// 	echo json_encode( [ 'status' => 0, 'message' => pll__( 'ASL_PHONE_ALREADY_TAKEN' ) ] );
				// 	wp_die();
				// }

				// create user
				$pass = wp_generate_password( 8, false );
				$uid = wp_insert_user( [
					'user_pass' => $pass,
					'user_login' => sanitize_email( $_POST['email'] ),
					'role' => 'customer',
					'user_email' => sanitize_email( $_POST['email'] ),
				] );

				if( is_wp_error( $uid ) ){
					// user not created
					echo json_encode( [ 'status' => 0, 'message' => $uid->get_error_message() ] );
					wp_die();
				}
				else{
					// send mail with credentials
					$to = sanitize_email( $_POST['email'] );
					$subject = 'abc-dieta. Анкета';
					$message = "<h3>Благодарим за заполение анкеты на abc-dieta!</h3> <p>Теперь Вы можете войти на сайт используя этот почтовый адрес и пароль <i>{$pass}</i>.</p> <p>Изменить пароль  другие учетные данные Вы можете в разделе <strong><a href='https://abc-dieta.com.ua/mij-akkaunt/''>\"Мой аккаунт\"</a></strong>.</p> <p>Спасибо что выбрали abc-dieta!</p>";
					$headers = 'From: ABC-dieta <noreply@abc-dieta.com.ua>' . "\r\n";
					$headers .= 'content-type: text/html; charset=utf-8' . "\r\n";
					wp_mail( $to, $subject, $message, $headers );

					// fill meta phone
					update_user_meta( $uid, 'billing_phone', sanitize_text_field( $_POST['phone'] ) );
					update_user_meta( $uid, 'shipping_phone', sanitize_text_field( $_POST['phone'] ) );

					// create coupon and bind it to the user
					$coupon_code = md5( sanitize_text_field( $_POST['phone'] ) ).'_'.$uid;
					$coupon_amount = 5;
					$discount_type = 'percent';

					$coupon_id = wp_insert_post( [
						'post_title' => $coupon_code,
						'post_content' => '',
						'post_status' => 'publish',
						'post_author' => 1,
						'post_type' => 'shop_coupon',
					] );

					update_post_meta( $coupon_id, 'discount_type', $discount_type );
					update_post_meta( $coupon_id, 'coupon_amount', $coupon_amount );

					// bind coupon to the user
					update_user_meta( $uid, 'coupon_code', $coupon_code );
				}
				
				echo json_encode( [ 'status' => 1, 'message' => pll__( 'SUCCESS_MESSAGE' ) ] );
			}
		}
	}

	wp_die();
}

add_action( 'wp_enqueue_scripts', 'as_questionnaire_enqueue_scripts' );
function as_questionnaire_enqueue_scripts(){
	wp_enqueue_script( 'as-questionnaire', QUESTIONNAIRE_URL . 'assets/js/questionnaire.js', [ 'jquery' ] );
}

add_action( 'woocommerce_before_cart', 'asq_apply_user_coupon' );
add_action( 'woocommerce_calculate_totals', 'asq_apply_user_coupon', 10, 1 );
function asq_apply_user_coupon() {
	if( is_user_logged_in() ){
		$coupon_code = get_user_meta( get_current_user_id(), 'coupon_code', true );
	    if ( WC()->cart->has_discount( $coupon_code ) ) return;
	    WC()->cart->add_discount( $coupon_code );
	    // wc_print_notices();
	}
}

?>