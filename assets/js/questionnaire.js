( function( $ ){
	$( document ).ready( function(){
		console.log( 'questionnaire here' );

		$( '#save_questionnaire_btn' ).on( 'click', function( e ){
			e.preventDefault();
			e.stopPropagation();
			
			$.ajax( {
				method: 'post',
				url: 'https://abc-dieta.com.ua/wp-admin/admin-ajax.php',
				dataType: 'json',
				data: $( '#questionnaire' ).serialize()
			} ).done( function( r ){
				console.log( r );
				$( '#questionnaire' ).fadeOut();
				$( '#questionnaire-result' ).text( r.message ).removeClass( 'alert-success' ).removeClass( 'alert-danger' ).addClass( 1 == r.status ? 'alert-success' : 'alert-danger' ).fadeIn();
				// setTimeout( () => { window.location.href = '/'; }, 2500);
			} );
		} );

	} );
} )( jQuery );