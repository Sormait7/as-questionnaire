<?php 
/**
 * Plugin Name:  Анкета клиента
 * Plugin URI:   https://actualsolutions.net/
 * Description:  Опрос клиентов и регистрация после опроса
 * Version:      1.0.0
 * Author:       Paul Strelkovsky
 */

defined( 'ABSPATH' ) or die();
define( 'QUESTIONNAIRE_URL', plugin_dir_url( __FILE__ ) );

// registering post type for questions
require_once( 'inc/posttypes.php' );

// registering polylang
require_once( 'inc/strings.php' );

// registering actions and handlers
require_once( 'inc/actions.php' );